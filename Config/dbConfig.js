'use strict';

var mongo = {
    URI: process.env.MONGODB_URI || 'mongodb://localhost/booholDev',
    port: 27017
};


module.exports = {
    mongo: mongo
};