'use strict';
var md5 = require('md5');

var Service = require('../Services');
var createUser = function (payload, callback) {
    var userObjToSave = {
        name: payload.name,
        email: payload.email,
        password: md5(payload.password),
    };
    Service.User.createUser(userObjToSave, function (err, data) {
        callback(err, data);
    });
};

var createAndSaveAccessToken = function (userId, callback) {
    var token = userId + new Date().getTime();
    token = md5(token);
    Service.User.updateUser({ _id: userId }, { $set: { accessToken: token } }, function (err, data) {
        if (err) {
            callback(err)
        } else {
            callback(null, token)
        }
    })
}

var getList = function (callback) {
    var criteria = {};
    Service.User.getUser(criteria, {password:0, accessToken:0, deviceToken:0}, {lean:true, sort :{lastUpdated : -1}}, function (err, data) {
        console.log('get all users',err,data)
        callback(err, data)
    })
};

var loginUser = function (payload, callback) {
    var criteria = {
        email: payload.email,
        password: md5(payload.password),
    }
    Service.User.getUser(criteria, {_id:1}, { lean: true }, function (err, data) {
        if (err) {
            callback(err)
        } else {
            if (data && data.length) {
                createAndSaveAccessToken(data[0]._id, function (err, token) {
                    if (err) {
                        callback(err)
                    } else {
                        callback(null, { token: token })
                    }

                })
            }else {
                callback('Invalid Username of password')
            }
        }
    })
};

var updateLocation = function (payload, callback) {
    var criteria = {
        accessToken : payload.token
    }
    var dataToEnter = [payload.lat, payload.long, new Date().getTime()];
    var dataToUpdate = {
        $set : {currentLocation : dataToEnter, lastUpdated : new Date().toISOString()},
        $push : {previousLocations : dataToEnter}
    }
    Service.User.updateUser(criteria, dataToUpdate,{}, function (err, data) {
        console.log('data>>>',data)
        if (err) {
            callback(err)
        } else {
            if (data && data.email) {
                callback()
            }else {
                callback('Invalid Token')
            }
        }
    })
};

var deleteUser = function (payload, callback) {
    var criteria = {
        email : payload.email
    }
    Service.User.deleteUser(criteria, function (err, data) {
        console.log('data>>>',data)
        if (err) {
            callback(err)
        } else {
            callback()
        }
    })
};

module.exports = {
    createUser: createUser,
    loginUser: loginUser,
    updateLocation: updateLocation,
    deleteUser: deleteUser,
    getList: getList
};