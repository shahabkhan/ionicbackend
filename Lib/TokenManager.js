'use strict';
/**
 * Created by shahab on 11/7/15.
 */
var Config = require('../Config');
var Jwt = require('jsonwebtoken');
var async = require('async');

var getTokenFromDB = function (userId, callback) {
    //TODO hit the DB here to check if the userId exists or not and return back userData
    var successResponse = {
        valid:true,
        userData:'USER_DATA_HERE'
    };
    callback(successResponse);
};

var setTokenInDB = function (userId,tokenToSave, callback) {
    //TODO hit the db to update the token

    callback();
};

var expireTokenInDB = function (key, callback) {
    if (key != null) {
       //TODO hit the db and set token to null
        callback();
    }
};

var verifyToken = function (token, callback) {
    var response = {
        valid : false
    };
    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            response.name = err.name
            callback(response)
        } else {
            getTokenFromDB(decoded.id, function (err, data) {
                if (!err && data && data == token) {
                    response.valid = true;
                    response.userData = decoded;
                }
                callback(response);
            });
        }
    });
};

var setToken = function (tokenData, callback) {
    if (!tokenData.id){
        callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.IMP_ERROR);
    }else {
        var tokenToSend = Jwt.sign(tokenData, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY);
        setTokenInDB(tokenData.id, tokenToSend, function (err,data) {
            callback(err,{accessToken:tokenToSend})
        })
    }
};

var expireToken = function (token, callback) {
    Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decoded) {
        if (err) {
            callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
        } else {
            expireTokenInDB(decoded.id, function (err, data) {
                callback(err,data)
            });
        }
    });
};

var decodeToken = function (token, callback) {
  Jwt.verify(token, Config.APP_CONSTANTS.SERVER.JWT_SECRET_KEY, function (err, decodedData) {
      if (err){
          callback(Config.APP_CONSTANTS.STATUS_MSG.ERROR.INVALID_TOKEN);
      }else {
          callback(null,decodedData)
      }
  })
};

module.exports = {
    expireToken: expireToken,
    setToken: setToken,
    verifyToken: verifyToken,
    decodeToken: decodeToken
};