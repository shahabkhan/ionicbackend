/**
 * Created by shahab on 10/7/15.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Config = require('../Config');

var Users = new Schema({
    name: {type: String, required: true, trim : true, index : true},
    email: {type: String, required: true, trim : true, index: true, unique : true},
    password : {type: String, required:true},
    accessToken : {type : String, trim: true, index: true, unique: true, sparse: true},
    deviceToken : {type : String, trim: true, index: true, unique: true, sparse: true},
    lastUpdated : {type : Date},
    currentLocation : {type: Array, default : [0,0]},
    previousLocations : {type: Array, default : []},
    profilePicURL : {
        original : {type : String},
        thumbnail : {type : String}
    }
});


module.exports = mongoose.model('Users', Users);