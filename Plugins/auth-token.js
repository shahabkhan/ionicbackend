'use strict';
/**
 * Created by shahab on 12/7/15.
 */

var TokenManager = require('../Lib/TokenManager');
var UniversalFunctions = require('../Utils/UniversalFunctions');

exports.register = function(server, options, next){

//Register Authorization Plugin
    server.register(require('hapi-auth-bearer-token'), function (err) {
        server.auth.strategy('UserAuth', 'bearer-access-token', {
            allowQueryToken: false,
            allowMultipleHeaders: true,
            accessTokenName: 'accessToken',
            validateFunc: function (token, callback) {
                TokenManager.verifyToken(token, function (response) {
                    if (response.valid){
                        callback(null, true, {token: token, userData: response.userData})
                    }else {
                        var errResponse;
                        if (response.name == 'TokenExpiredError'){
                            errResponse = {
                                errorMessage : UniversalFunctions.CONFIG.APP_CONSTANTS.STATUS_MSG.ERROR.TOKEN_ALREADY_EXPIRED
                            }
                        }else {
                            errResponse = null
                        }
                        callback(errResponse, false, {token: token, userData: null})
                    }
                });

            }
        });
    });

    next();
};

exports.register.attributes = {
    name: 'auth-token-plugin'
};