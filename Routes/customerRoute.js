'use strict';
/**
 * Created by shahab on 10/7/15.
 */

var Controller = require('../Controllers');
var UniversalFunctions = require('../Utils/UniversalFunctions');
var Joi = require('joi');

module.exports = [
    {
        method: 'POST'
        , path: '/api/user/register'
        , handler: function (request, reply) {
            Controller.UserController.createUser(request.payload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        }, config: {
            description: 'Register User'
            , tags: ['api', 'user']
            , validate: {
                payload: {
                    name: Joi.string().regex(/^[a-zA-Z ]+$/).required().trim(),
                    email: Joi.string().email().required(),
                    password: Joi.string().required().min(6)
                }
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'POST'
        , path: '/api/user/login'
        , handler: function (request, reply) {
            Controller.UserController.loginUser(request.payload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        }, config: {
            description: 'Login User'
            , tags: ['api', 'user']
            , validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().required().min(6)
                }
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'PUT'
        , path: '/api/user/updateLocation'
        , handler: function (request, reply) {
            Controller.UserController.updateLocation(request.payload, function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        }, config: {
            description: 'Update Location'
            , tags: ['api', 'user']
            , validate: {
                payload: {
                    token: Joi.string().required(),
                    lat: Joi.number().required(),
                    long: Joi.number().required()
                }
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'GET'
        , path: '/api/user/getList'
        , handler: function (request, reply) {
            Controller.UserController.getList(function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        }, config: {
            description: 'Get List of users Location'
            , tags: ['api', 'user'],
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    },
    {
        method: 'DELETE'
        , path: '/api/user/deleteUser'
        , handler: function (request, reply) {
            Controller.UserController.deleteUser(request.payload,function (err, data) {
                if (err) {
                    reply(UniversalFunctions.sendError(err));
                } else {
                    reply(UniversalFunctions.sendSuccess(null, data))
                }
            });
        }, config: {
            description: 'Delete User'
            , tags: ['api', 'user'],
            validate: {
                payload: {
                    email: Joi.string().required()
                }
            },
            plugins: {
                'hapi-swagger': {
                    responseMessages: UniversalFunctions.CONFIG.APP_CONSTANTS.swaggerDefaultResponseMessages
                }
            }
        }
    }
];